package main

import (
	"encoding/binary"
	"fmt"
	"math"
	"os"
	"time"
)

//var sampleRate float64

const (
	SampleRate = 96100
	Frequency  = 440 //hz Pitch Standard
)

func main() {
	metronome(90, 4)
}

func generateSound(duration, sampleRate int, frequency float64) {
	var (
		start float64 = 3.0
		end   float64 = 1.0e-4
	)

	tau := math.Pi * 2
	nsamp := SampleRate * duration
	var angle float64 = tau / float64(nsamp)

	fileName := "out.bin"
	f, _ := os.Create(fileName)

	decayfac := math.Pow(end/start, 1.0/float64(nsamp))

	fmt.Printf("%v\n", decayfac)

	for i := 0; i < nsamp; i++ {
		// making sample and convert it to sine
		sample := math.Sin(angle * frequency * float64(i))
		//decay the sound
		sample *= start
		start *= decayfac

		var buf [8]byte
		binary.LittleEndian.PutUint32(buf[:], math.Float32bits(float32(sample)))
		// here I need to play the sound
		bw, err := f.Write(buf[:])
		if err != nil {
			fmt.Println(err.Error())
		}

		fmt.Printf("\rWrote: %v bytes to %s", bw, fileName)
	}
}

func metronome(bpm float64, timeSignature int) {
	// set to false when you want to turn off bpm
	isBpmOff := false

	//create a new ticker (interval)and a way to exit the interval
	tick := time.NewTicker(time.Minute / time.Duration(bpm))
	quit := make(chan struct{})

	//running goroutine so the metronome will run on the backround
	go func(timeSignature int) {
		n := 1
		isGenerated := false

		for {
			if isBpmOff {
				quit <- struct{}{}
				close(quit)
			}
			select {
			case <-tick.C:
				//generateSound(1)
				if n > timeSignature {
					n = 1
					if !isGenerated {
						go generateSound(2, SampleRate, Frequency) // generate higher pitch sound
						isGenerated = true
					}
				}
				fmt.Printf("Click %v\n", n)
				n = n + 1
			case <-quit:
				tick.Stop()
				return
			}
		}
	}(timeSignature)

	//while the bpm is on keep the bpm on!
	for !isBpmOff {
		time.Sleep(time.Second)
	}
}
